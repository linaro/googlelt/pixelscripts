# Building the upstream kernel for Pixel 6 / 6 Pro

This README details how to build and flash an upstream kernel
to a Pixel 6 / 6 Pro phone.

## Get the kernel source

**Note**: If you are setting up your build environment for the first time,
follow the steps in the section [Install host dependencies](#install-host-dependencies).

```
repo init -u https://git.codelinaro.org/linaro/googlelt/manifest.git -b main
repo sync -j64 --jobs-checkout=$(nproc) -c --no-tags
```

## Compile and flash

To build and package the kernel images, run the command:

```
make images
```

To flash the kernel, [put your device in fastboot mode](#setup-the-phone-for-flashing)
and run the command:

```
make flash
```

This will prepare the device for development, flash and then boot the images:
```
out/images/boot.img
out/images/vendor_boot.img
```

By default, the device will boot to a minimal BusyBox-based console using
a prebuilt initramfs. To boot to a more complete root file system that supports
autoloading kernel modules via udev, refer to the section
[Configuring the rootfs](#configuring-the-rootfs).

**Note:** Since upstream doesn't support devicetree overlays in the same way
Android does, we package the full devicetree blobs (dtbs) with the
`vendor_boot.img` and erase the `dtbo` partition.

For more advanced `make` operations, take a look at the additional `make`
targets by running `make help`.

### Rebooting the device with the upstream kernel

Currently, rebooting the device via the `reboot` command is not supported.
Instead of holding the power button for a minute, you can reboot the device by
executing the following command from the serial console:

```
echo b > /proc/sysrq-trigger
```

Alternatively you can reboot via SysRq over the serial line by sending
\<BREAK\>-r. When using minicom, BREAK is sent via Meta-F.


### Configuring the rootfs

To boot to a console, a root file system is required. During boot, an initramfs
usually is loaded first which then mounts the root file system from a partition
in flash to continue the boot from there.

The default `make` targets use a prebuilt minimal BusyBox-based initramfs that
also acts as the sole root file system. It is included in the repo clone.

To create a more complete rootfs using the [Yocto](https://www.yoctoproject.org/)
build environment, follow the steps below. This rootfs can include additional
command line tools and support automatic kernel module loading, amongst others.
We can subsequently point `make` at that instead.

#### rootfs creation via the Yocto build environment

1) Sync our OpenEmbedded repo manifest:

```
repo init -u https://git.codelinaro.org/linaro/googlelt/manifest.git -b oe/master -m default.xml
repo sync -j64 --jobs-checkout=$(nproc) -c --no-tags
```

2) Create the Yocto build directory using the `setup-environment` script. For
hermeticity purposes, this needs to be outside of /usr. Since in some
environments $HOME is below /usr, the example below initialises the build
directory to be below `/yocto-builds/yocto/`.

```
sudo mkdir -p /yocto-builds/yocto
sudo chown -R ${USER}:$(id -gn ${USER}) /yocto-builds
MACHINE=google-gs DISTRO=poky EXTRA_LAYERS= . ./setup-environment /yocto-builds/yocto/build-google-gs-poky
```

**Note**: The `setup-environment` script needs to be sourced before invoking
bitbake each time a new shell is entered. On subsequent invocations of
`setup-environment`, i.e. once the build directory exists, the additional
arguments (`DISTRO`, `MACHINE`, etc.) do not need to be passed-in anymore.

3) Build the Yocto-based rootfs of your choice. Thanks to the above
configuration, this will also build the kernel and the initramfs. The example
creates the `core-image-full-cmdline` rootfs, but other options exist:

```
umask 0022
bitbake core-image-full-cmdline
```

This creates a couple of artefacts in the `tmp/deploy/images/google-gs` directory.
Take note of the rootfs and initramfs images:

```
$ ls -1 /yocto-builds/yocto/build-google-gs-poky/tmp/deploy/images/google-gs/
...
core-image-full-cmdline-google-gs.rootfs.ext4
...
googlegs-initramfs-image-google-gs.cpio.gz
...
```

**Tip**: Refer to the [Yocto documentation](https://docs.yoctoproject.org/), in
particular
[Customizing Images Using local.conf](https://docs.yoctoproject.org/dev-manual/customizing-images.html#customizing-images-using-local-conf),
for instuctions on how to add additional packages to that rootfs if desired.

#### flashing the Yocto-built rootfs

The rootfs needs to be flashed to the `userdata` partition of the device.

**Note**: doing so will irrevocably wipe anything Android normally stores
in `userdata`, like photos and documents and downloads and possibly other
application data. Please be careful and do a backup first if that data
is valuable.

```
fastboot flash userdata \
    /yocto-builds/yocto/build-google-gs-poky/tmp/deploy/images/google-gs/tmp/deploy/images/google-gs/core-image-full-cmdline-google-gs.rootfs.ext4
```

#### pointing this build environment at the Yocto artefacts

1) The kernel that is to be built as part of this build environment here needs to
be told:

* where to find the real rootfs in flash (`userdata` partition)
* to copy the kernel modules from this build into the real rootfs as part of boot

2) Additionally, we have to point this build environment at the Yocto-built
initramfs, so that we can boot the Yocto-rootfs using the kernel and kernel-
modules from this build environment here.


The first point should be automatic, since the Makefile automatically adds the
necessary kernel command line options if a non-default initramfs location was
detected:

```
ifneq ($(PREBUILT_CPIO),$(PREBUILT_CPIO_BUSYBOX))
KERNELCMDLINE += root=PARTLABEL=userdata copy_modules
endif
```

For the second point, the initramfs can be found at the path:
```
/yocto-builds/yocto/build-google-gs-poky/tmp/deploy/images/google-gs/googlegs-initramfs-image-google-gs.cpio.gz
```

To use the newly built Yocto initramfs, inject it into make using the
`PREBUILT_CPIO` make variable, e.g.:

```
make \
    PREBUILT_CPIO=/yocto-builds/yocto/build-google-gs-poky/tmp/deploy/images/google-gs/googlegs-initramfs-image-google-gs.cpio.gz \
    flash
```

or

```
export PREBUILT_CPIO=/yocto-builds/yocto/build-google-gs-poky/tmp/deploy/images/google-gs/googlegs-initramfs-image-google-gs.cpio.gz
make flash
```

**Tip**: See the section [Makefile tips](#makefile-tips) below for options to
simplify dealing with the `PREBUILT_CPIO` variable.


## Setup your build environment and Pixel device

### Install host dependencies:

```
sudo apt-get install bison flex file gcc-aarch64-linux-gnu libssl-dev lld lz4 pigz rsync sparse
sudo apt install python3 python3-dev python3-venv swig
python3 -m venv venv
. venv/bin/activate
pip3 install dtschema rfc3987 yamllint
```

**Note**: To update your packages in future, run the following command:

```
pip3 install --upgrade \
    dtschema rfc3987 yamllint \
    $(pip3 list --outdated --disable-pip-version-check \
      | tail -n +3 \
      | awk ' { print $1 } ')
```

### Setup the phone for flashing

As the upstream kernel support is currently quite limited a [USB-Cereal](https://www.crowdsupply.com/0xda/usb-cereal)
device is required to obtain the serial console access and interact with
the kernel.

This should be connected to the phone and splits the debug UART from the
ADB/fastboot connection. Both ADB and UART should be connected to the host
computer.

**Note 1**: Select 1.8v voltage selection on USB-Cereal (small switch on left side)

**Note 2**: USB-Cereal has no orientation detection. If you don't have a working
serial console, change the orientation of the dongle or cable connecting to the
phone.

**Note 3**: The USB connectors on previous debug dongles have known to be quite
fragile with repeated plugging and unplugging. To avoid issues we usually use a
set of 3x usb-c cables so the dongle does not need to be repeadedly plugged amd
unplugged. This setup avoids unnecessary strain on the USB-Cereal connectors.

The image below shows an example cable setup, with the phone in fastboot mode.

![Pixel 6 Setup](imgs/p21cables.jpg)

### Entering fastboot

Now ensure the Pixel phone is connected via USB and is in fastboot mode
ready for flashing new images.

To enter fastboot mode, **press and hold the power and volume down keys
simultaneously** in the bootloader when the phone first turns on. Once
you've entered fastboot mode the phone will display a red triangle and
say "fastboot mode" in red writing amongst other information. Refer to
the image above.

It is always possible to reboot into fastboot mode (for example from
a crashed development kernel) **by doing a long (60seconds or so) power and
volume down key press**. This will then allow a new kernel to be flashed.


## Additional information

### Makefile tips

When deciding what to do, `make` inspects the following files, in this
order: `GNUmakefile`, `makefile`, `Makefile`. We can use this behaviour to
affect the build without having a diff on the Makefile or having to export
variables into the environment or into make, by adding all custom configuration
to `GNUmakefile`. To use this approach, create `GNUmakefile` at the root of the
repo workspace:

```
$ cat <<EOF > GNUmakefile
# yocto initramfs
PREBUILT_CPIO := /yocto-builds/yocto/build-google-gs-poky/tmp/deploy/images/google-gs/googlegs-initramfs-image-google-gs.cpio.gz

include ./Makefile
EOF
```

This way, we permanently override the built-in default for the initramfs and
always use the Yocto-built one.

Similarly, this file can also contain other related confiuration, e.g. for
fastboot:

```
$ cat <<EOF > GNUmakefile
# fastboot via network
export ANDROID_SERIAL := tcp:localhost:35799
export PATH := ${HOME}/devel/google/android-master/out/host/linux-x86/bin:${PATH}

# yocto initramfs
PREBUILT_CPIO := /yocto-builds/yocto/build-google-gs-poky/tmp/deploy/images/google-gs/googlegs-initramfs-image-google-gs.cpio.gz

include ./Makefile
EOF
```

This allows us to still just type `make run`.


### Bootloader notes

Some versions of the Pixel 6 bootloader crash when no DTBO exists in
flash. A version that is known to work is: slider-15.2-12357666 from
[Android 15 QPR2 Beta 1 build 12620009](https://flash.android.com/preview/vic-qpr2-beta1):

```
$ fetch_artifact --use_oauth2 --bid 12620009 \
                 --target oriole_beta-userdebug \
                 bootloader.img
$ fastboot flash bootloader bootloader.img
$ fastboot reboot-bootloader

$ fastboot getvar version-bootloader
version-bootloader: slider-15.2-12357666
```

Other bootloader versions that are known to not crash are:
* slider-1.3-11000330
* slider-1.2-8739948
* slider-1.2-9152140

## Example UART log

```
Reboot Info:
  RST_STAT: 0xc0000 - PIN_RESET | PO_RESET
  GSA_RESET_STATUS: 0x1 - GSA_PINRESET
  Reboot reason: 0xcccc - Long Press Power Key Reboot (MASTER DISCONNECT)
  Reboot mode: 0x0 - Normal Boot
[   2.794641] [I] LK version: Little-Kernel-2.0-g
[   2.794700] [I] LK build date: Jun 22 2023, time: 00:16:33
[   2.794777] [I] BL version: slider-1.3-10362403
[   2.805899] [E] [DSIM] timeout!! wait for DSIM interrupt (0x0)
[   2.805973] [E] [DSIM] MIPI DSIM (id=6) read Timeout! 0x4
[   2.806046] [E] [DSIM] dsim fail to get panel operation
[   2.806141] [E] [DSIM] dsim has error.
[   2.806190] [E] [DSIM] dsim_probe -: dsim1 probe failed.
[   2.806259] [I] [DSIM] dsim1 probe was failed
[   2.806319] [I] [DSIM] dsim_init -: set dsim0 driver as primary.
starting app pixel_loader
[   2.808819] [I] [MAX77759] TCPC BC1.2, type: 1 val:1
[   2.810647] [I] [PXL] boot voltage threshold=3400mV
[   2.810915] [I] [RMDMP] Ramdump process result: ibh magic=0x72644842 flag=0x11(WRITE|ERROR) error=1(interrupted) time=0 secs
[   2.811059] [I] [PXL] bootloader message:boot-fastboot
[   2.816754] [I] [PXL] fastboot enter reason: vol down pressed
[   2.816861] [E] [DSIM] dsim0 wait phy pll stable
[   2.816918] [E] [DSIM] PHY clock lane is not ready[timeout]
[   2.816987] [E] [DSIM] PHY data lane0 is not ready[timeout]
[   2.817059] [E] [DSIM] PHY data lane1 is not ready[timeout]
[   2.817130] [E] [DSIM] Error to enable PHY lane(err=3)
[   2.817203] [I] [DPU] target_display_primary_reset: BTYPE
starting app pixel_charger
[   2.818371] [I] [MAX77759] TCPC BC1.2, type: 1 val:1
[   2.832382] [I] [DPP] dpp_probe +
[   2.832428] [I] [DPP] dpp-0's attr is (0x00050047)
[   2.832491] [I] [DPP] dpp(0) init resources.
[   2.832570] [I] [DPP] dpp_probe -: dpp0 is probed successfully
[   2.832650] [I] [DECON] decon-int: max win6, command mode, hw trigger
[   2.832738] [I] [DECON] single dsi mode
[   2.832804] [I] [decon0] d_path(0xB1) DSCC DSC_ENC1 DSC_ENC0 OFIFO.0
[   2.832910] [I] [decon0] sel(0x0) OFIFO.0 - dsim0 - samsung-s6e3fc3
[   2.833078] [I] [DECON] Display 1st window(5) xres(1080) yres(2400) win_start_pos(0) win_end_pos(95f0437)
[   2.885759] [I] [TCPM] port connected
[   2.885916] [I] [MAX77759] TCPC BC1.2, type: 1 val:1
[   2.979753] [I] [DECON] decon0 registered successfully
[   3.006703] [I] [MODEM] version: g5123b-120690-230620-B-10349564
[   3.038699] [I] [PHY] USB Pipe3 Initialization...
[   3.059494] [I] [MAX77759] TCPC BC1.2, type: 1 val:1
[   3.651532] [I] [MAX77759] TCPC BC1.2, type: 1 val:1
[   3.679102] [I] [UDC] new high-speed device found
[   3.679340] [I] [MAX77759] TCPC BC1.2, type: 1 val:1
[  17.438980] [I] [MAX77759] TCPC BC1.2, type: 1 val:1
[  17.440308] [I] [MAX77759] TCPC BC1.2, type: 1 val:1
[  17.460729] [I] [GS] halt action=1, reason=8
[  17.610395] [I] [UFS] shutdown complete
Done
     
DMC:10362403-880d9a0-20230622001534

 starting dmc initialization ========
     [000.000018]      check wakeup status 
     [000.000377]      soc preset 
     [000.001393]      get timing parameter - 50MHz 
     [000.018110]      preset for identification 
     [000.022385]      examine MRW 
     [000.025496]      identyfy DRAM - 0041000860000110
     [000.027405]      get timing parameter - target freq. 
     [000.037175]      low freq init. 
     [000.047977]      dvfs to boot freq. 
     [000.047930]      high freq. init. 
     [000.049234]      offset dq training 
     [000.114813]      advanced read training 
     [000.703043]      advanced write training 
     [001.491180]      DCA training 
     [001.487021]      all level read training 
     [001.580286]      all level write training 
     [001.630034]      all level PRBS training 
     [001.996786]      post settings 
     [002.066318]      init. done 
     [002.064418]      sanity check 
     [002.074213]      dmc initialization finished 
     [002.074231]      DMC Done 


GSA Load FW Res: 00000000
10362403-8c374b3--20230622001649
[   0.000000] [I] [LDFW] 3 loaded out of 3
secure os: boot args 0x*** 0x*** 0x1000 0x0
secure os: gicc 0x***, gicd 0x***, gicr 0x***
secure os: mte is not available
secure os: initializing trusty (Project: slider, Build: 10238280, Built: 07:04:17 Jun  1 2023)
[   0.000000] [I] [GS] secure payload loading took 24ms
[   0.000000] [I] [GS] platform info binded: false
[   0.000000] [I] [GS] platform info: 020306010000
[   2.652332] [I] [MAIN] 
welcome to lk/MP

[   2.652383] [I] [MAIN] boot args 0x1 0x0 0xcb000411 0x0
[   2.652989] [I] [GS] Soc rev: B1
[   2.653031] [I] [GS] Soc ID: 0x0a778fabe830
[   2.653095] [I] [GS] DVFS version: 10
[   2.653172] [I] [PMIC] S2MPG10 CHIPID 0x5
[   2.653252] [I] [PMIC] S2MPG11 CHIPID 0x6
[   2.653328] [I] [PMIC] S2MPG10 PWRONSRC 0x80
[   2.653408] [I] [PMIC] S2MPG10 OFFSRC 0x20
[   2.653486] [I] [PMIC] S2MPG10 RTC_WTSR 0x7e
[   2.653566] [I] [PMIC] S2MPG11 OFFSRC 0x21
[   2.653642] [I] [PMIC] S2MPG11 INT1 0x3
[   2.653720] [I] [PMIC] S2MPG11 INT2 0x20
[   2.653796] [I] [PMIC] S2MPG11 INT3 0x0
[   2.653932] [I] [PMIC] S2MPG11 INT4 0x0
[   2.654075] [I] [PMIC] S2MPG11 INT5 0x0
[   2.654218] [I] [PMIC] S2MPG11 INT6 0x0
[   2.654337] [I] [PMIC] S2MPG10 INT4 0x0
[   2.654480] [I] [PMIC] S2MPG10 INT5 0x0
[   2.654666] [I] [DPM] dpm version 101
[   2.654767] [E] [LNXDBG] magic_number must be 0xcceeddff but 0x00000000
[   2.655014] [E] [LNXDBG] vendor magic_number must be 0xcceeddff but 0x00000000
APC Bootmetrics:
  BL1: MT:1 / DT:ENTRY / 44424 us
  BL1: MT:2 / DT:LOAD  / 3237 us
  BL1: MT:F / DT:EXIT  / 51778 us
  PBL: MT:1 / DT:ENTRY / 51845 us
  PBL: MT:2 / DT:LOAD  / 28769 us
  PBL: MT:F / DT:EXIT  / 85608 us
  BL2: MT:1 / DT:ENTRY / 392298 us
  BL2: MT:2 / DT:LOAD  / 98 us
  BL2: MT:4 / DT:CHECK / 580158 us
  BL2: MT:5 / DT:CHECK / 1439 us
  BL2: MT:6 / DT:LOAD  / 9181 us
  BL2: MT:7 / DT:LOAD  / 20869 us
  BL2: MT:8 / DT:LOAD  / 17 us
  BL2: MT:F / DT:EXIT  / 2460032 us
  PBL: MT:3 / DT:LOAD  / 71025 us
  PBL: MT:4 / DT:LOAD  / 41529 us
  PBL: MT:5 / DT:LOAD  / 3563 us
  EL3: MT:1 / DT:ENTRY / 2603676 us
  EL3: MT:F / DT:EXIT  / 2614923 us
APC Breadcrumbs(v2.1):
 stages(S): 00000000 / 00004040
  flags(S): 00000000 / 00000000
stages(NS): 00000000 / 00005004
 flags(NS): 00000000 / 00000000
    status: 00000000 / 00000000
    device: 00000000 / 00000001
     reset: 00000000 / 000C0000
[   2.658445] [E] [DBGC] GSA Breadcrumb:
0xffff0000f8aebe30: 00000000 0000000d 00000000 00000000 |................|
0xffff0000f8aebe40: 00000000 00000000 00000001          |................|
invalid ps entry at 0xffff0000f8aebb94
[   2.659179] [E] [GPT] partition misc not found
failed to read misc(vendor) partition -2
[   2.659592] [I] [dev] initializing device i2c:usi12
[   2.659669] [I] [I2C] usi12: version 2.2
[   2.659829] [I] [dev] initializing device i2c:usi6
[   2.659995] [I] [I2C] usi6: version 2.2
[   2.660146] [I] [dev] initializing device i2c:usi7
[   2.660322] [I] [I2C] usi7: version 2.2
[   2.660472] [I] [dev] initializing device i2c:usi8
[   2.660640] [I] [I2C] usi8: version 2.2
[   2.663497] [I] [UFS] UFS EVT version 1
[   2.663557] [I] [UFS] controller version 3.0 found
[   2.663620] [I] [UFS] capabilities 0x1303ff1f
[   2.663861] [I] [UFS] UFS device found
[   2.663906] [E] [UFS] ufshc_notify done
[   2.663990] [I] [UFS] ufshc_power_mode_change gear 4
[   2.674124] [I] [UFS] ufshc: bBootLunEn 0x1
[   2.674221] [I] [UFS] UFS device version 3.1
[   2.674272] [I] [UFS] Enabled LUNs found 4
[   2.674345] [I] [UFS] UFS device total size 127984992256
[   2.675213] [I] [UFS] Write Booster: available buffer=0x0 lifetime=0x1 current buffer=0xc00
[   2.675339] [I] [SLIDER] boot slot: a
[   2.678264] [E] [GPT] partition pinfo not found
[   2.678318] [E] failed to read pinfo partition -2
[   2.678378] [E] pinfo data will not persist
[   2.678431] [E] [GPT] partition pinfo not found
[   2.678490] [E] failed to write pinfo partition -2
[   2.678560] [I] [SLIDER] platform info: 020306010000
[   2.679098] [I] [MG5] FG device name 0x6200
[   2.679291] [I] [MG5] FG is NOT initialized
[   2.679888] [I] [MG5] SOC(100), VCELL(4276), INOW(-608), IAVG(-53)
[   2.682651] [I] [MAX77759] tcpc vendor id 6a
[   2.684472] [I] [MAX20339] revision 0x02
[   2.684603] [I] [MAX20339] OVLO set to 14.5
[   2.685443] [I] [BMS] master disconnect reason 0x0
[   2.685863] [I] [dev] initializing device cs40l25:cs40l25
[   2.686628] [I] [CS40L25] CS40L25 found
[   2.687762] [I] [FUSE] LCS state:PRODUCTION
[   2.687824] [I] [SLIDER] saving ddr training data
[   2.689226] [I] [FFU] no valid file
[   2.695003] [I] [dev] initializing device nfc:st21nfc
[   2.695081] [I] [SLIDER] [TMU:0] 43
[   2.695125] [I] [SLIDER] [TMU:1] 42
[   2.717525] [I] [DSIM] dsim_probe dsim0+
[   2.717579] [I] [DSIM] using data lane count(4)
[   2.727770] [E] [DSIM] dsim0 wait phy pll stable
[   2.727828] [E] [DSIM] PHY clock lane is not ready[timeout]
[   2.727896] [E] [DSIM] PHY data lane0 is not ready[timeout]
[   2.727963] [E] [DSIM] PHY data lane1 is not ready[timeout]
[   2.728030] [E] [DSIM] PHY data lane2 is not ready[timeout]
[   2.728097] [E] [DSIM] Error to enable PHY lane(err=4)
[   2.728171] [I] [DPU] target_display_primary_reset: BTYPE
[   2.744352] [I] [DPU] Suceeded to read panel id : 0x0040803a
[   2.744421] [E] [DPU] is_compatible is not defined!
[   2.744481] [I] [DPU] panel_rev: 0x800
[   2.754560] [I] [DSIM] use panel samsung-s6e3fc3 for dsim0
[   2.754628] [I] [DSIM] dsim_probe -: dsim0 driver(cmd mode) has been probed.
[   2.754712] [I] [DSIM] dsim_probe dsim1+
[   2.754763] [I] [DSIM] using data lane count(4)
[   2.754841] [E] [DSIM] dsim1 wait phy pll stable
[   2.754898] [E] [DSIM] PHY clock lane is not ready[timeout]
[   2.754964] [E] [DSIM] PHY data lane0 is not ready[timeout]
[   2.755091] [E] [DSIM] PHY data lane1 is not ready[timeout]
[   2.755301] [E] [DSIM] Error to enable PHY lane(err=3)
[   2.794597] [E] [SLIDER] 
Reboot Info:
  RST_STAT: 0xc0000 - PIN_RESET | PO_RESET
  GSA_RESET_STATUS: 0x1 - GSA_PINRESET
  Reboot reason: 0xcafe - User Reboot(S/W Reboot)
  Reboot mode: 0x0 - Normal Boot
[   2.794847] [I] LK version: Little-Kernel-2.0-g
[   2.794906] [I] LK build date: Jun 22 2023, time: 00:16:33
[   2.794983] [I] BL version: slider-1.3-10362403
[   2.806104] [E] [DSIM] timeout!! wait for DSIM interrupt (0x0)
[   2.806177] [E] [DSIM] MIPI DSIM (id=6) read Timeout! 0x4
[   2.806248] [E] [DSIM] dsim fail to get panel operation
[   2.806341] [E] [DSIM] dsim has error.
[   2.806391] [E] [DSIM] dsim_probe -: dsim1 probe failed.
[   2.806459] [I] [DSIM] dsim1 probe was failed
[   2.806519] [I] [DSIM] dsim_init -: set dsim0 driver as primary.
starting app pixel_loader
[   2.809017] [I] [MAX77759] TCPC BC1.2, type: 1 val:1
[   2.810821] [I] [PXL] boot voltage threshold=3400mV
[   2.811088] [I] [RMDMP] Ramdump process result: ibh magic=0x72644842 flag=0x11(WRITE|ERROR) error=1(interrupted) time=0 secs
[   2.811233] [I] [PXL] bootloader message:boot-fastboot
[   2.817299] [I] [MAX77759] TCPC BC1.2, type: 1 val:1
[   2.819087] [I] [PXL] boot voltage threshold=3400mV
[   2.819344] [E] [DSIM] dsim0 wait phy pll stable
[   2.819405] [E] [DSIM] PHY clock lane is not ready[timeout]
[   2.819472] [E] [DSIM] PHY data lane0 is not ready[timeout]
[   2.819539] [E] [DSIM] PHY data lane1 is not ready[timeout]
[   2.819606] [E] [DSIM] PHY data lane2 is not ready[timeout]
[   2.819673] [E] [DSIM] Error to enable PHY lane(err=4)
[   2.819749] [I] [DPU] target_display_primary_reset: BTYPE
[   2.821084] avb_vbmeta_image.c:207: ERROR: Hash does not match!
[   2.821170] avb_slot_verify.c:760: ERROR: vbmeta_a: Error verifying vbmeta image: HASH_MISMATCH
[   2.834912] [I] [DPP] dpp_probe +
[   2.834953] [I] [DPP] dpp-0's attr is (0x00050047)
[   2.835011] [I] [DPP] dpp(0) init resources.
[   2.835078] [I] [DPP] dpp_probe -: dpp0 is probed successfully
[   2.835153] [I] [DECON] decon-int: max win6, command mode, hw trigger
[   2.835231] [I] [DECON] single dsi mode
[   2.835285] [I] [decon0] d_path(0xB1) DSCC DSC_ENC1 DSC_ENC0 OFIFO.0
[   2.835384] [I] [decon0] sel(0x0) OFIFO.0 - dsim0 - samsung-s6e3fc3
[   2.835604] [I] [DECON] Display 1st window(5) xres(1080) yres(2400) win_start_pos(0) win_end_pos(95f0437)
[   2.841528] [E] [AVB] avb_property(com.android.build.system.os_version): not found
[   2.841622] [E] [AVB] system.os_version: can't find property
[   2.841705] [E] [AVB] avb_property(com.android.build.system.security_patch): not found
[   2.841804] [E] [AVB] system_spl: can't find property
[   2.841869] [E] [AVB] avb_property(com.android.build.boot.security_patch): not found
[   2.841960] [E] [AVB] boot_spl: can't find property
[   2.842140] [I] [AVB] boot_state: 000000:20000000:20000000
trusty_dev_common.c: INFO selected trusty api version: 3 (requested 3)
secure os: ARM TRNG total time for 8 bytes: *** ns
secure os: _Bool sm_check_and_lock_api_version(uint32_t):127: min api version set: 2
[   2.892340] [I] [TCPM] port connected
[   2.892499] [I] [AND] kernel: addr:0xffff0000a0001000, size:1340869
[   2.975001] [I] [AND] vendor ramdisk size 0x99f512(10089746), type gzip
[   2.979071] [I] [DTBO] SOC DTB: index:0
[   2.979129] [I] [DTBO] DTBO match: id 0x20304, rev 0x10000
[   2.980339] [E] [DPM] cannot find path /dpm
[   2.980398] [I] [AND] dpm not merged
[   2.980596] [I] [AND] bootmode:recovery
[   2.980712] [I] [AND] console is enabled (3000000n8)
[   2.980781] [I] [BOOT] secure dram base 0xb7200000, size 0x8e00000
[   2.980880] [I] [BOOT] secure PGTBL base 0xe0000000, size 38797312
[   2.984296] [I] [BOOT] Connector class: /hsi2c@10D50000/max77759tcpc/connector path not found (-1)
[   2.984572] [I] [BOOT] Connector class: /hsi2c@10D50000/max77759tcpc/connector path not found (-1)
[   3.009717] [I] [AND] kernel: total_time:117
[   3.009953] [I] [FDT] device: /hsi2c@10D50000/max77729chrg path not found (-1)
[   3.010199] [I] [FDT] device: /hsi2c@10D50000/max77729fg path not found (-1)
[   3.010445] [I] [FDT] device: /hsi2c@10D50000/max77729uic path not found (-1)
[   3.028626] [I] [DECON] decon0 registered successfully
[   3.030068] [I] [DPU] panel settings after overriding: samsung-s6e3fc3@dsim0 null@dsim1
[  13.087801] [I] [UFS] shutdown complete
[  13.087855] [E] [PSTORE] unable to get pram
Heap dump (using miniheap):
        base 0xffff0000f8b43000, len 0x338000
        free list:
                base 0xffff0000f8b744f8, end 0xffff0000f8b74540, len 0x48
                base 0xffff0000f8b74aa0, end 0xffff0000f8b75158, len 0x6b8
                base 0xffff0000f8b75778, end 0xffff0000f8bc3538, len 0x4ddc0
                base 0xffff0000f8d11558, end 0xffff0000f8d91958, len 0x80400
                base 0xffff0000f8d91a08, end 0xffff0000f8d91a28, len 0x20
                base 0xffff0000f8dd1de0, end 0xffff0000f8de21e0, len 0x10400
                base 0xffff0000f8de2390, end 0xffff0000f8de23b0, len 0x20
                base 0xffff0000f8e74650, end 0xffff0000f8e7b000, len 0x69b0
[  13.089194] [I] [AND] Starting Linux kernel ...
[    0.000000] Booting Linux on physical CPU 0x0000000000 [0x412fd050]
[    0.000000] Linux version 6.6.0-rc1-00021-g931e1ea32aea (gpeter@gpeter-l) (aarch64-linux-gnu-gcc (Debian 13.2.0-2) 13.2.0, GNU ld (GNU Binutils for Debian) 2.41) #3 SMP PREEMPT Thu Oct  5 11:25:11 BST 2023
[    0.000000] KASLR enabled
[    0.000000] Machine model: Oriole DVT
[    0.000000] earlycon: exynos4210 at MMIO32 0x0000000010a00000 (options '')
[    0.000000] printk: bootconsole [exynos4210] enabled
[    0.000000] printk: debug: ignoring loglevel setting.
[    0.000000] WARNING: 'bootconfig' found on the kernel command line but CONFIG_BOOT_CONFIG is not set.
[    0.000000] efi: UEFI not found.
[    0.000000] OF: reserved mem: 0x0000000090200000..0x00000000905fffff (4096 KiB) nomap non-reusable gsa@90200000
[    0.000000] OF: reserved mem: 0x0000000093000000..0x0000000093ffffff (16384 KiB) nomap non-reusable tpu_fw@93000000
[    0.000000] OF: reserved mem: 0x0000000094000000..0x0000000096ffffff (49152 KiB) nomap non-reusable aoc@94000000
[    0.000000] OF: reserved mem: 0x00000000f8800000..0x00000000f97fffff (16384 KiB) nomap non-reusable abl@f8800000
[    0.000000] OF: reserved mem: 0x00000000fd3f0000..0x00000000fd3fdfff (56 KiB) nomap non-reusable dss_log_reserved@fd3f0000
[    0.000000] OF: reserved mem: 0x00000000fd3fe000..0x00000000fd3fefff (4 KiB) nomap non-reusable debug_kinfo_reserved@fd3fe000
[    0.000000] OF: reserved mem: 0x00000000fd800000..0x00000000fd8fffff (1024 KiB) nomap non-reusable bldr_log_reserved@fd800000
[    0.000000] OF: reserved mem: 0x00000000fd900000..0x00000000fd901fff (8 KiB) nomap non-reusable bldr_log_hist_reserved@fd900000
[    0.000000] OF: reserved mem: 0x00000000fd90a000..0x00000000fd90bfff (8 KiB) nomap non-reusable pbl_log_reserved
[    0.000000] NUMA: No NUMA configuration found
[    0.000000] NUMA: Faking a node at [mem 0x0000000080000000-0x00000009ffffffff]
[    0.000000] NUMA: NODE_DATA [mem 0x9ff0479c0-0x9ff049fff]
[    0.000000] Zone ranges:
[    0.000000]   DMA      [mem 0x0000000080000000-0x00000000ffffffff]
[    0.000000]   DMA32    empty
[    0.000000]   Normal   [mem 0x0000000100000000-0x00000009ffffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000080000000-0x00000000901fffff]
[    0.000000]   node   0: [mem 0x0000000090200000-0x00000000905fffff]
[    0.000000]   node   0: [mem 0x0000000090600000-0x0000000092ffffff]
[    0.000000]   node   0: [mem 0x0000000093000000-0x0000000096ffffff]
[    0.000000]   node   0: [mem 0x0000000097000000-0x00000000b71fffff]
[    0.000000]   node   0: [mem 0x00000000c0000000-0x00000000dfffffff]
[    0.000000]   node   0: [mem 0x00000000e2500000-0x00000000f87fffff]
[    0.000000]   node   0: [mem 0x00000000f8800000-0x00000000f97fffff]
[    0.000000]   node   0: [mem 0x00000000f9800000-0x00000000fd3effff]
[    0.000000]   node   0: [mem 0x00000000fd3f0000-0x00000000fd3fefff]
[    0.000000]   node   0: [mem 0x00000000fd3ff000-0x00000000fd7fffff]
[    0.000000]   node   0: [mem 0x00000000fd800000-0x00000000fd901fff]
[    0.000000]   node   0: [mem 0x00000000fd902000-0x00000000fd909fff]
[    0.000000]   node   0: [mem 0x00000000fd90a000-0x00000000fd90bfff]
[    0.000000]   node   0: [mem 0x00000000fd90c000-0x00000000ffffffff]
[    0.000000]   node   0: [mem 0x0000000880000000-0x00000009ffffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000080000000-0x00000009ffffffff]
[    0.000000] On node 0, zone DMA: 3584 pages in unavailable ranges
[    0.000000] On node 0, zone DMA: 9472 pages in unavailable ranges
[    0.000000] cma: Reserved 32 MiB at 0x00000000fe000000 on node -1
[    0.000000] psci: probing for conduit method from DT.
[    0.000000] psci: PSCIv1.1 detected in firmware.
[    0.000000] psci: Using standard PSCI v0.2 function IDs
[    0.000000] psci: MIGRATE_INFO_TYPE not supported.
[    0.000000] psci: SMC Calling Convention v1.1
[    0.000000] percpu: Embedded 22 pages/cpu s50664 r8192 d31256 u90112
[    0.000000] pcpu-alloc: s50664 r8192 d31256 u90112 alloc=22*4096
[    0.000000] pcpu-alloc: [0] 0 [0] 1 [0] 2 [0] 3 [0] 4 [0] 5 [0] 6 [0] 7 
[    0.000000] Detected VIPT I-cache on CPU0
[    0.000000] CPU features: SYS_ID_AA64MMFR1_EL1[11:8]: forced to 0
[    0.000000] CPU features: detected: GIC system register CPU interface
[    0.000000] CPU features: kernel page table isolation forced ON by KASLR
[    0.000000] CPU features: detected: Kernel page table isolation (KPTI)
[    0.000000] CPU features: detected: Qualcomm erratum 1009, or ARM erratum 1286807, 2441009
[    0.000000] CPU features: detected: ARM errata 1165522, 1319367, or 1530923
[    0.000000] alternatives: applying boot alternatives
[    0.000000] Kernel command line: earlycon=exynos4210,mmio32,0x10A00000 clk_ignore_unused ignore_loglevel earlycon=exynos4210,0x10A00000 clk_ignore_unused console=ttySAC0,3000000n8 exynos_drm.panel_name=samsung-s6e3fc3 tcpci_max77759.conf_sbu=1 id_aa64mmfr1.vh=0 kvm-arm.mode=none bootconfig
[    0.000000] random: crng init done
[    0.000000] Dentry cache hash table entries: 1048576 (order: 11, 8388608 bytes, linear)
[    0.000000] Inode-cache hash table entries: 524288 (order: 10, 4194304 bytes, linear)
[    0.000000] Fallback order for Node 0: 0 
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 2019276
[    0.000000] Policy zone: Normal
[    0.000000] mem auto-init: stack:all(zero), heap alloc:off, heap free:off
[    0.000000] software IO TLB: area num 8.
[    0.000000] software IO TLB: mapped [mem 0x00000000f4800000-0x00000000f8800000] (64MB)
[    0.000000] Memory: 7809892K/8205312K available (16448K kernel code, 4264K rwdata, 10556K rodata, 9152K init, 597K bss, 362652K reserved, 32768K cma-reserved)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=8, Nodes=1
[    0.000000] rcu: Preemptible hierarchical RCU implementation.
[    0.000000] rcu:     RCU event tracing is enabled.
[    0.000000] rcu:     RCU restricting CPUs from NR_CPUS=256 to nr_cpu_ids=8.
[    0.000000]  Trampoline variant of Tasks RCU enabled.
[    0.000000]  Tracing variant of Tasks RCU enabled.
[    0.000000] rcu: RCU calculated value of scheduler-enlistment delay is 25 jiffies.
[    0.000000] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=8
[    0.000000] NR_IRQS: 64, nr_irqs: 64, preallocated irqs: 0
[    0.000000] GICv3: GIC: Using split EOI/Deactivate mode
[    0.000000] GICv3: 960 SPIs implemented
[    0.000000] GICv3: 0 Extended SPIs implemented
[    0.000000] Root IRQ handler: gic_handle_irq
[    0.000000] GICv3: GICv3 features: 16 PPIs
[    0.000000] GICv3: CPU0: found redistributor 0 region 0:0x0000000010440000
[    0.000000] rcu: srcu_init: Setting srcu_struct sizes based on contention.
[    0.000000] samsung_clk_register_gate: failed to register clock gout_cmu_misc_bus
[    0.000000] samsung_clk_register_gate: failed to register clock gout_cmu_misc_sss
[    0.000000] arch_timer: cp15 timer(s) running at 24.57MHz (phys).
[    0.000000] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x5ab00a189, max_idle_ns: 440795202599 ns
[    0.000000] sched_clock: 56 bits at 25MHz, resolution 40ns, wraps every 4398046511099ns
[    0.000422] Console: colour dummy device 80x25
[    0.000551] Calibrating delay loop (skipped), value calculated using timer frequency.. 49.15 BogoMIPS (lpj=98304)
[    0.000679] pid_max: default: 32768 minimum: 301
[    0.000799] LSM: initializing lsm=capability,integrity
[    0.000938] Mount-cache hash table entries: 16384 (order: 5, 131072 bytes, linear)
[    0.001129] Mountpoint-cache hash table entries: 16384 (order: 5, 131072 bytes, linear)
[    0.002257] cacheinfo: Unable to detect cache hierarchy for CPU 0
[    0.002727] RCU Tasks: Setting shift to 3 and lim to 1 rcu_task_cb_adjust=1.
[    0.002851] RCU Tasks Trace: Setting shift to 3 and lim to 1 rcu_task_cb_adjust=1.
[    0.003055] rcu: Hierarchical SRCU implementation.
[    0.003117] rcu:     Max phase no-delay instances is 1000.
[    0.004685] EFI services will not be available.
[    0.004911] smp: Bringing up secondary CPUs ...
[    0.005442] Detected VIPT I-cache on CPU1
[    0.005489] GICv3: CPU1: found redistributor 100 region 0:0x0000000010460000
[    0.005515] CPU1: Booted secondary processor 0x0000000100 [0x412fd050]
[    0.005970] Detected VIPT I-cache on CPU2
[    0.006007] GICv3: CPU2: found redistributor 200 region 0:0x0000000010480000
[    0.006032] CPU2: Booted secondary processor 0x0000000200 [0x412fd050]
[    0.006445] Detected VIPT I-cache on CPU3
[    0.006476] GICv3: CPU3: found redistributor 300 region 0:0x00000000104a0000
[    0.006499] CPU3: Booted secondary processor 0x0000000300 [0x412fd050]
[    0.007190] CPU features: detected: Spectre-v4
[    0.007194] CPU features: detected: Spectre-BHB
[    0.007197] Detected PIPT I-cache on CPU4
[    0.007224] GICv3: CPU4: found redistributor 400 region 0:0x00000000104c0000
[    0.007244] CPU4: Booted secondary processor 0x0000000400 [0x414fd0b0]
[    0.007684] Detected PIPT I-cache on CPU5
[    0.007715] GICv3: CPU5: found redistributor 500 region 0:0x00000000104e0000
[    0.007735] CPU5: Booted secondary processor 0x0000000500 [0x414fd0b0]
[    0.008441] Detected PIPT I-cache on CPU6
[    0.008473] GICv3: CPU6: found redistributor 600 region 0:0x0000000010500000
[    0.008495] CPU6: Booted secondary processor 0x0000000600 [0x411fd440]
[    0.008964] Detected PIPT I-cache on CPU7
[    0.008999] GICv3: CPU7: found redistributor 700 region 0:0x0000000010520000
[    0.009019] CPU7: Booted secondary processor 0x0000000700 [0x411fd440]
[    0.009070] smp: Brought up 1 node, 8 CPUs
[    0.013456] SMP: Total of 8 processors activated.
[    0.013612] CPU features: detected: 32-bit EL0 Support
[    0.013804] CPU features: detected: Data cache clean to the PoU not required for I/D coherence
[    0.014133] CPU features: detected: Common not Private translations
[    0.014373] CPU features: detected: CRC32 instructions
[    0.014570] CPU features: detected: RCpc load-acquire (LDAPR)
[    0.014788] CPU features: detected: LSE atomic instructions
[    0.015031] CPU features: detected: Privileged Access Never
[    0.015214] CPU features: detected: RAS Extension Support
[    0.015425] CPU features: detected: Speculative Store Bypassing Safe (SSBS)
[    0.015796] CPU: All CPU(s) started at EL2
[    0.015858] alternatives: applying system-wide alternatives
[    0.020287] devtmpfs: initialized
[    0.023301] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 7645041785100000 ns
[    0.023425] futex hash table entries: 2048 (order: 5, 131072 bytes, linear)
[    0.023786] pinctrl core: initialized pinctrl subsystem
[    0.024559] DMI not present or invalid.
[    0.024828] NET: Registered PF_NETLINK/PF_ROUTE protocol family
[    0.025305] DMA: preallocated 1024 KiB GFP_KERNEL pool for atomic allocations
[    0.025527] DMA: preallocated 1024 KiB GFP_KERNEL|GFP_DMA pool for atomic allocations
[    0.025744] DMA: preallocated 1024 KiB GFP_KERNEL|GFP_DMA32 pool for atomic allocations
[    0.025861] audit: initializing netlink subsys (disabled)
[    0.025986] audit: type=2000 audit(0.024:1): state=initialized audit_enabled=0 res=1
[    0.026429] thermal_sys: Registered thermal governor 'step_wise'
[    0.026432] thermal_sys: Registered thermal governor 'power_allocator'
[    0.026527] cpuidle: using governor menu
[    0.026743] hw-breakpoint: found 6 breakpoint and 4 watchpoint registers.
[    0.026915] ASID allocator initialised with 32768 entries
[    0.027712] Serial: AMBA PL011 UART driver
[    0.028487] gpio gpiochip0: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.028701] gpio gpiochip1: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.028850] gpio gpiochip2: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.029002] gpio gpiochip3: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.029150] gpio gpiochip4: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.029378] gpio gpiochip5: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.029743] gpio gpiochip6: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.030081] gpio gpiochip7: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.030713] gpio gpiochip8: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.030870] gpio gpiochip9: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.031085] gpio gpiochip10: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.031443] gpio gpiochip11: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.031890] samsung-pinctrl 17a80000.pinctrl: irq number not available
[    0.031976] gpio gpiochip12: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.032375] gpio gpiochip13: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.032719] gpio gpiochip14: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.033165] samsung-pinctrl 17940000.pinctrl: irq number not available
[    0.033250] gpio gpiochip15: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.033823] gpio gpiochip16: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.033981] gpio gpiochip17: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.034578] gpio gpiochip18: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.034731] gpio gpiochip19: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.035024] gpio gpiochip20: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.036155] gpio gpiochip21: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.036314] gpio gpiochip22: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.036465] gpio gpiochip23: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.036617] gpio gpiochip24: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.036767] gpio gpiochip25: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.037085] gpio gpiochip26: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.037430] gpio gpiochip27: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.037770] gpio gpiochip28: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.038114] gpio gpiochip29: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.038455] gpio gpiochip30: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.038799] gpio gpiochip31: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.039140] gpio gpiochip32: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.039487] gpio gpiochip33: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.039814] gpio gpiochip34: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.040169] gpio gpiochip35: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.040512] gpio gpiochip36: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.040855] gpio gpiochip37: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.041199] gpio gpiochip38: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.041543] gpio gpiochip39: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.041881] gpio gpiochip40: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.042668] gpio gpiochip41: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.042823] gpio gpiochip42: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.042973] gpio gpiochip43: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.043254] gpio gpiochip44: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.043594] gpio gpiochip45: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.043919] gpio gpiochip46: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.044257] gpio gpiochip47: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.044622] gpio gpiochip48: Static allocation of GPIO base is deprecated, use dynamic allocation.
[    0.046403] exynos_arm64_register_cmu: could not enable bus clock dout_misc_bus; err = -2
[    0.047287] Modules: 2G module region forced by RANDOMIZE_MODULE_REGION_FULL
[    0.047372] Modules: 0 pages in range for non-PLT usage
[    0.047374] Modules: 513984 pages in range for PLT usage
[    0.047712] HugeTLB: registered 1.00 GiB page size, pre-allocated 0 pages
[    0.047857] HugeTLB: 0 KiB vmemmap can be freed for a 1.00 GiB page
[    0.047934] HugeTLB: registered 32.0 MiB page size, pre-allocated 0 pages
[    0.048015] HugeTLB: 0 KiB vmemmap can be freed for a 32.0 MiB page
[    0.048108] HugeTLB: registered 2.00 MiB page size, pre-allocated 0 pages
[    0.048367] HugeTLB: 0 KiB vmemmap can be freed for a 2.00 MiB page
[    0.048607] HugeTLB: registered 64.0 KiB page size, pre-allocated 0 pages
[    0.048866] HugeTLB: 0 KiB vmemmap can be freed for a 64.0 KiB page
[    0.049622] ACPI: Interpreter disabled.
[    0.050099] iommu: Default domain type: Translated
[    0.050157] iommu: DMA domain TLB invalidation policy: strict mode
[    0.050316] SCSI subsystem initialized
[    0.050407] libata version 3.00 loaded.
[    0.050519] usbcore: registered new interface driver usbfs
[    0.050596] usbcore: registered new interface driver hub
[    0.050672] usbcore: registered new device driver usb
[    0.050884] pps_core: LinuxPPS API ver. 1 registered
[    0.050948] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.051110] PTP clock support registered
[    0.051283] EDAC MC: Ver: 3.0.0
[    0.051540] scmi_core: SCMI protocol bus registered
[    0.051763] FPGA manager framework
[    0.051832] Advanced Linux Sound Architecture Driver Initialized.
[    0.052235] vgaarb: loaded
[    0.052429] clocksource: Switched to clocksource arch_sys_counter
[    0.052570] VFS: Disk quotas dquot_6.6.0
[    0.052630] VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    0.052769] pnp: PnP ACPI: disabled
[    0.054901] NET: Registered PF_INET protocol family
[    0.055060] IP idents hash table entries: 131072 (order: 8, 1048576 bytes, linear)
[    0.057710] tcp_listen_portaddr_hash hash table entries: 4096 (order: 4, 65536 bytes, linear)
[    0.057872] Table-perturb hash table entries: 65536 (order: 6, 262144 bytes, linear)
[    0.057971] TCP established hash table entries: 65536 (order: 7, 524288 bytes, linear)
[    0.058164] TCP bind hash table entries: 65536 (order: 9, 2097152 bytes, linear)
[    0.058549] TCP: Hash tables configured (established 65536 bind 65536)
[    0.058664] UDP hash table entries: 4096 (order: 5, 131072 bytes, linear)
[    0.058769] UDP-Lite hash table entries: 4096 (order: 5, 131072 bytes, linear)
[    0.058916] NET: Registered PF_UNIX/PF_LOCAL protocol family
[    0.059212] RPC: Registered named UNIX socket transport module.
[    0.059297] RPC: Registered udp transport module.
[    0.059477] RPC: Registered tcp transport module.
[    0.059656] RPC: Registered tcp-with-tls transport module.
[    0.059895] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    0.060144] PCI: CLS 0 bytes, default 64
[    0.060390] Unpacking initramfs...
[    0.064683] kvm [1]: KVM disabled from command line
[    0.065098] Initialise system trusted keyrings
[    0.065204] workingset: timestamp_bits=42 max_order=21 bucket_order=0
[    0.065381] squashfs: version 4.0 (2009/01/31) Phillip Lougher
[    0.065528] NFS: Registering the id_resolver key type
[    0.065593] Key type id_resolver registered
[    0.065645] Key type id_legacy registered
[    0.065699] nfs4filelayout_init: NFSv4 File Layout Driver Registering...
[    0.065779] nfs4flexfilelayout_init: NFSv4 Flexfile Layout Driver Registering...
[    0.065968] 9p: Installing v9fs 9p2000 file system support
[    0.076334] Key type asymmetric registered
[    0.076385] Asymmetric key parser 'x509' registered
[    0.076464] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 245)
[    0.076553] io scheduler mq-deadline registered
[    0.076608] io scheduler kyber registered
[    0.076670] io scheduler bfq registered
[    0.079089] EINJ: ACPI disabled.
[    0.084555] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    0.085528] [    0.085626] 10a00000.serial: ttySAC0 at MMIO 0x10a00000 (irq = 20, base_baud = 0) is a S3C6400/10
[    0.085742] printk: console [ttySAC0] enabled
[    0.085742] printk: console [ttySAC0] enabled
[    0.085847] printk: bootconsole [exynos4210] disabled
[    0.085847] printk: bootconsole [exynos4210] disabled
[    0.086087] SuperH (H)SCI(F) driver initialized
[    0.086233] msm_serial: driver initialized
[    0.086450] STM32 USART driver initialized
[    0.089248] loop: module loaded
[    0.089532] megasas: 07.725.01.00-rc1
[    0.091102] tun: Universal TUN/TAP device driver, 1.6
[    0.091362] thunder_xcv, ver 1.0
[    0.091409] thunder_bgx, ver 1.0
[    0.091455] nicpf, ver 1.0
[    0.091747] hns3: Hisilicon Ethernet Network Driver for Hip08 Family - version
[    0.091835] hns3: Copyright (c) 2017 Huawei Corporation.
[    0.091906] hclge is initializing
[    0.091959] e1000: Intel(R) PRO/1000 Network Driver
[    0.092019] e1000: Copyright (c) 1999-2006 Intel Corporation.
[    0.092094] e1000e: Intel(R) PRO/1000 Network Driver
[    0.092155] e1000e: Copyright(c) 1999 - 2015 Intel Corporation.
[    0.092331] igb: Intel(R) Gigabit Ethernet Network Driver
[    0.092539] igb: Copyright (c) 2007-2014 Intel Corporation.
[    0.092751] igbvf: Intel(R) Gigabit Virtual Function Network Driver
[    0.092984] igbvf: Copyright (c) 2009 - 2012 Intel Corporation.
[    0.093276] sky2: driver version 1.30
[    0.093642] VFIO - User Level meta-driver version: 0.3
[    0.094275] usbcore: registered new interface driver usb-storage
[    0.094955] i2c_dev: i2c /dev entries driver
[    0.096010] s3c2410-wdt 10060000.watchdog: stopping watchdog timer
[    0.096161] s3c2410-wdt 10060000.watchdog: watchdog inactive, reset disabled, irq disabled
[    0.096261] s3c2410-wdt 10060000.watchdog: DBGACK enabled
[    0.096328] s3c2410-wdt 10060000.watchdog: windowed watchdog disabled, wtmincnt=8000
[    0.097279] sdhci: Secure Digital Host Controller Interface driver
[    0.097354] sdhci: Copyright(c) Pierre Ossman
[    0.097550] Synopsys Designware Multimedia Card Interface Driver
[    0.097815] sdhci-pltfm: SDHCI platform and OF driver helper
[    0.098355] ledtrig-cpu: registered to indicate activity on CPUs
[    0.099040] usbcore: registered new interface driver usbhid
[    0.099107] usbhid: USB HID core driver
[    0.100001] hw perfevents: enabled with armv8_pmuv3 PMU driver, 7 counters available
[    0.101205] NET: Registered PF_PACKET protocol family
[    0.101294] 9pnet: Installing 9P2000 support
[    0.101362] Key type dns_resolver registered
[    0.104401] registered taskstats version 1
[    0.104527] Loading compiled-in X.509 certificates
[    0.111561] gpio-keys gpio_keys: Unable to get irq number for GPIO 0, error -6
[    0.111742] clk: Not disabling unused clocks
[    0.111797] ALSA device list:
[    0.111835]   No soundcards found.
[    0.320265] Freeing initrd memory: 9852K
[    0.321860] Freeing unused kernel memory: 9152K
[    0.360795] Run /init as init process
[    0.360840]   with arguments:
[    0.360876]     /init
[    0.360908]   with environment:
[    0.360950]     HOME=/
[    0.360983]     TERM=linux

Please press Enter to activate this console. 
/ # cat /proc/cpuinfo 
processor       : 0
BogoMIPS        : 49.15
Features        : fp asimd evtstrm aes pmull sha1 sha2 crc32 atomics fphp asimdhp cpuid asimdrdm lrcpc dcpop asimddp
CPU implementer : 0x41
CPU architecture: 8
CPU variant     : 0x2
CPU part        : 0xd05
CPU revision    : 0

processor       : 1
BogoMIPS        : 49.15
Features        : fp asimd evtstrm aes pmull sha1 sha2 crc32 atomics fphp asimdhp cpuid asimdrdm lrcpc dcpop asimddp
CPU implementer : 0x41
CPU architecture: 8
CPU variant     : 0x2
CPU part        : 0xd05
CPU revision    : 0

processor       : 2
BogoMIPS        : 49.15
Features        : fp asimd evtstrm aes pmull sha1 sha2 crc32 atomics fphp asimdhp cpuid asimdrdm lrcpc dcpop asimddp
CPU implementer : 0x41
CPU architecture: 8
CPU variant     : 0x2
CPU part        : 0xd05
CPU revision    : 0

processor       : 3
BogoMIPS        : 49.15
Features        : fp asimd evtstrm aes pmull sha1 sha2 crc32 atomics fphp asimdhp cpuid asimdrdm lrcpc dcpop asimddp
CPU implementer : 0x41
CPU architecture: 8
CPU variant     : 0x2
CPU part        : 0xd05
CPU revision    : 0

processor       : 4
BogoMIPS        : 49.15
Features        : fp asimd evtstrm aes pmull sha1 sha2 crc32 atomics fphp asimdhp cpuid asimdrdm lrcpc dcpop asimddp
CPU implementer : 0x41
CPU architecture: 8
CPU variant     : 0x4
CPU part        : 0xd0b
CPU revision    : 0

processor       : 5
BogoMIPS        : 49.15
Features        : fp asimd evtstrm aes pmull sha1 sha2 crc32 atomics fphp asimdhp cpuid asimdrdm lrcpc dcpop asimddp
CPU implementer : 0x41
CPU architecture: 8
CPU variant     : 0x4
CPU part        : 0xd0b
CPU revision    : 0

processor       : 6
BogoMIPS        : 49.15
Features        : fp asimd evtstrm aes pmull sha1 sha2 crc32 atomics fphp asimdhp cpuid asimdrdm lrcpc dcpop asimddp
CPU implementer : 0x41
CPU architecture: 8
CPU variant     : 0x1
CPU part        : 0xd44
CPU revision    : 0

```

PARALLEL ?= $(shell nproc)

OUT ?= out

# try to use schedtool & ionice (i.e. if installed)
TRY_SCHED ?= 1

# this provides / uses a minimal prebuilt cpio image, but an alternative
# may be passed in via the PREBUILT_CPIO variable
PREBUILT_CPIO ?= $(PREBUILT_CPIO_BUSYBOX)


# shouldn't have to change anything below this line
KERNELCMDLINE := ignore_loglevel
KERNELCMDLINE += earlycon
KERNELCMDLINE += console=ttySAC0,3000000n8
KERNELCMDLINE += clk_ignore_unused
KERNELCMDLINE += printk.devkmsg=on
KERNELCMDLINE += log_buf_len=1024K
KERNELCMDLINE += at24.write_timeout=100
KERNELCMDLINE += init_fatal_sh
ifneq ($(PREBUILT_CPIO),$(PREBUILT_CPIO_BUSYBOX))
KERNELCMDLINE += root=PARTLABEL=userdata copy_modules
endif
KERNELCMDLINE += adbd

MAKEFLAGS += --no-builtin-rules

.PHONY: all
all: help

.PHONY: help
help:
	@echo "Build and optionally flash and/or boot Pixel 6 images"
	@echo "The following make targets exist:"
	@echo "    help              this help"
	@echo "    clean             clean all generated files"
	@echo ""
	@echo "    prepare-device    prepare device for development (Verity/UART/boot"
	@echo "                      partitions/etc.)"
	@echo "    run               directly boot (run) images on (potentially rebuilding"
	@echo "                      images first)"
	@echo "                      Note: this will bypass any potentially flashed images"
	@echo "    rerun             directly boot (run) images (without rebuilding)"
	@echo "                      Note: this will bypass any potentially flashed images"
	@echo "    flash             flash and boot images (potentially rebuilding them first)"
	@echo "    reflash           flash and boot images (without rebuilding)"
	@echo "    flash-no-boot     flash but don't boot images (potentially rebuilding them"
	@echo "                      first)"
	@echo "    reflash-no-boot   flash but don't boot images (without rebuilding)"
	@echo ""
	@echo "    images            generate all flashable images (including kernel artefacts)"
	@echo "    build             build all kernel artefacts (only)"
	@echo ""
	@echo "    menuconfig        Linux menuconfig"
	@echo "    nconfig           Linux nconfig"
	@echo "    savedefconfig     Linux savedefconfig"
	@echo "    copydefconfig     savedefconfig and copy back into Linux source tree"
	@echo ""
	@echo "    dt_binding_check_vendors"
	@echo "    dt_binding_check_vendor_exynos"
	@echo "    dt_binding_check_vendor_google"
	@echo "    dt_binding_check_vendor_samsung"
	@echo "                      check only those DT bindings for vendors that we care"
	@echo "                      about for Pixel6."
	@echo "    dt_binding_check_all"
	@echo "                      check all DT bindings (this will likely complain about"
	@echo "                      unrelated bindings)"
	@echo "    dtbs_check_exynos validate all Samsung Exynos DTs (32 & 64 bit ARM)"
	@echo "    dtbs_check_raviole"
	@echo "                      validate Pixel6 DTs (using W=1 unless overridden)"
	@echo "    validate_dt       run checks of DT bindings and DTs related to Pixel6"
	@echo ""
	@echo ""
	@echo "Notes regarding image / boot / flash targets:"
	@echo "  * To boot an image, we need to know the DTB to use. Choices are:"
	@echo "    '$(POSSIBLE_RUNTARGETS)', and '$(RUNTARGET)' is currently in effect."
	@echo "    This can be changed by specifying the RUNTARGET make variable, e.g."
	@echo "        make RUNTARGET=oriole run"
	@echo "    or, as a shortcut, by specifying one of the possible RUNTARGETs as uppercase"
	@echo "    make variable, e.g."
	@echo "        make ORIOLE=1 run"
	@echo "    Specifying RUNTARGET= will take precedence over this shortcut."
	@echo "  * To boot an image, boot.img and vendor_boot.img should be in sync."
	@echo "      * the 'run' and 'rerun' targets upload new images and run those instead of"
	@echo "        flashing, they will not run any images that potentially have been"
	@echo "        flashed to the device previously"
	@echo "      * if boot and vendor_boot partitions have been erased (as is the case"
	@echo "        after the 'prepare-device' target), the device will always boot up into"
	@echo "        fastboot, waiting for a new image, thereby ensuring that it never boots"
	@echo "        an old (out-of-date) image"
	@echo "      * for the 'run' or 'rerun' targets to work, you should have run the"
	@echo "        'prepare-device' target at least once first. (It is implicitly executed"
	@echo "        as part of the 'flash' targets and therefore does not need to be"
	@echo "        explicitly executed for those)"
	@echo "  * Taking the above into account, the 'run' or 'rerun' targets are usually the"
	@echo "    correct targets to use"
	@echo ""
	@echo "Notes regarding DT bindings:"
	@echo "  * Specifying multiple targets should work, even with -j"
	@echo "  * dt_binding_check_vendors is mutually exclusive with"
	@echo "    dt_binding_check_vendor_*"
	@echo "  * dt_binding_check_vendor_samsung and dt_binding_check_vendor_exynos shouldn't"
	@echo "    be specified together when using -j, as they will trample on each other for"
	@echo "    some files at least"
	@echo "  * Sometimes it is necessary to update installed python packages to fix kernel"
	@echo "    DT validation errors. Please see the README.md for instructions on how to do"
	@echo "    that."
	@echo ""
	@echo "Notes regarding cpio images:"
	@echo "  * By default, this is building a minimal cpio initramfs image based on some"
	@echo "    prebuilt binaries. This might not be suitable in all cases, so alternatively"
	@echo "    the PREBUILT_CPIO make-variable can be used to point to a different cpio."
	@echo "    That cpio should be a fully useable initramfs file system as desired, A copy"
	@echo "    of that cpio will be modified to include kernel+modules and will then be"
	@echo "    used instead."
	@echo ""
	@echo "There are also a few internal targets, but you should not need to"
	@echo "invoke them manually, hence they're not described here."
	@echo ""
	@echo "Note: While building, we use schedtool and ionice (if available) to"
	@echo "      increase interactive responsiveness of the system."


.PHONY: clean
clean:
	rm -rf $(OUT)


THIS_MAKEFILE := $(lastword $(MAKEFILE_LIST))

ifeq ($(TRY_SCHED),1)
SCHED := $(shell command -pv schedtool)
ifneq ($(SCHED),)
SCHED += -B -n20 -e
endif # SCHED

IONICE := $(shell command -pv ionice)
ifneq ($(IONICE),)
IONICE += -c3
endif # IONICE
endif # TRY_SCHED

ifeq ($(KGZIP),)
KGZIP := pigz
endif # KGZIP

ifeq ($(origin LD),default)
LD := $(shell command -pv ld.lld)
ifeq ($(LD),)
LD := aarch64-linux-gnu-ld.bfd
endif # LD
endif # LD
STRIP := aarch64-linux-gnu-strip

# if sparse can be found in PATH, assign default CF= arguments if not given as SPARSE=
ifeq ($(SPARSE),)
ifneq ($(shell command -pv sparse),)
SPARSE := -fdiagnostic-prefix -D__CHECK_ENDIAN__ -fmax-errors=unlimited -fmax-warnings=unlimited
endif
endif
# prepare make command line (add C= and CF= as necessary)
SPARSE_BUILD_FLAGS :=
ifneq ($(SPARSE),)
C ?= 1
SPARSE_BUILD_FLAGS := C=$(C) CF='$(SPARSE)'
endif


MKDTIMG := tools/prebuilts/linux-x86/bin/mkdtimg
MKBOOTFS := tools/prebuilts/linux-x86/bin/mkbootfs
MKBOOTIMG := tools/mkbootimg/mkbootimg.py

POSSIBLE_RUNTARGETS := oriole raven

BOARDDTB ?= exynos/google/gs101-oriole.dtb exynos/google/gs101-raven.dtb
BOARDDTBS_OUT := $(addprefix $(OUT)/dtb/,$(notdir $(BOARDDTB)))

# RUNTARGET= checking
define runtarget_shortcut
  ifeq ($($(shell echo $(1) | tr '[[:lower:]]' '[[:upper:]]')),1)
  RUNTARGET += $(1)
  endif
endef

$(foreach trgt,$(POSSIBLE_RUNTARGETS),$(eval $(call runtarget_shortcut,$(trgt))))

RUNTARGET ?= oriole

ifneq ($(words $(RUNTARGET)),1)
$(error Too many RUNTARGETs given ($(RUNTARGET)), just pick *one*)
endif
ifeq ($(filter $(RUNTARGET), $(POSSIBLE_RUNTARGETS)),)
$(error RUNTARGET $(RUNTARGET) is unsupported, please pick one of: $(POSSIBLE_RUNTARGETS))
endif

EFFECTIVE_BOARDDTB_OUT := $(filter %$(RUNTARGET).dtb, $(BOARDDTBS_OUT))


LINUX ?= src/linux
KERNEL_OUT ?= $(shell realpath --relative-to=$(LINUX) $(OUT))
KERNEL_MAKE = $(MAKE) -C $(LINUX) $(SPARSE_BUILD_FLAGS) KGZIP=$(KGZIP) ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- LD='$(LD)' O=$(KERNEL_OUT)/linux

$(OUT)/FORCE:

KERNEL_STAMP := $(OUT)/.kernel.stamp
$(KERNEL_STAMP): $(OUT)/FORCE | $(OUT)
	export GIT_INDEX_FILE=$(KERNEL_OUT)/$(@F).gitindex \
	&& cd "$(LINUX)" \
	&& git_dir="$$(git rev-parse --git-dir)" \
	&& cp "$${git_dir}/index" "$${GIT_INDEX_FILE}" \
	&& git --git-dir="$${git_dir}" add -A . \
	&& git --git-dir="$${git_dir}" write-tree > $(KERNEL_OUT)/$(@F).tmp \
	&& rm "$${GIT_INDEX_FILE}"
	echo "$(KERNEL_MAKE)" >> $@.tmp
	if ! cmp -s '$@' '$@.tmp' ; then mv '$@.tmp' '$@' ; else rm '$@.tmp' ; fi

KERNEL_DOTCONFIG := $(OUT)/linux/.config
$(KERNEL_DOTCONFIG): src/pixelscripts/gs101_config.fragment | $(OUT)/linux
$(KERNEL_DOTCONFIG): $(KERNEL_STAMP)
	mkdir -p $(OUT)/linux/arch/arm64/configs
	KCONFIG_CONFIG=$(OUT)/linux/arch/arm64/configs/gs101_defconfig \
	    $(LINUX)/scripts/kconfig/merge_config.sh -m -r \
	        $(LINUX)/arch/arm64/configs/defconfig \
		src/pixelscripts/gs101_config.fragment
	+$(KERNEL_MAKE) gs101_defconfig

KERNEL_DOTCONFIG32 := $(OUT)/linux32/.config
$(KERNEL_DOTCONFIG32): $(KERNEL_STAMP)
	+$(KERNEL_MAKE) ARCH=arm O=$(KERNEL_OUT)/linux32 multi_v7_defconfig

.PHONY: configure
configure: $(KERNEL_DOTCONFIG)

.PHONY: copydefconfig menuconfig nconfig savedefconfig
menuconfig nconfig savedefconfig:
	+$(KERNEL_MAKE) $@

copydefconfig: savedefconfig
	@echo "Copying $(OUT)/linux/defconfig to $(LINUX)/arch/arm64/configs/defconfig"
	cp -a $(OUT)/linux/defconfig $(LINUX)/arch/arm64/configs/defconfig


BUILD_STAMP := $(OUT)/.kernel.built.stamp
$(BUILD_STAMP) $(OUT)/linux/arch/arm64/boot/Image &: $(KERNEL_STAMP) $(KERNEL_DOTCONFIG)
	@# The kernel sources could have changed without affecting the
	@# generated .config, i.e. our make executes this recipe because of
	@# KERNEL_STAMP, not because of KERNEL_DOTCONFIG. In that case our
	@# make will also rerun the KERNEL_DOTCONFIG target forever, as
	@# .config will never be up-to-date anymore (since Kbuild doesn't
	@# update .config in this case).
	@# To work-around that, if we get here, we know the .config is
	@# up-to-date, and the kernel will be regenerated anyway by Kbuild,
	@# so we might as well touch the .config, so that our make doesn't
	@# rerun the KERNEL_DOTCONFIG target.
	@# The implication is that Kbuild might regenerate
	@# include/config/auto.conf.cmd unnecessarily, but hopefully that
	@# doesn't matter for build time (auto.conf.cmd shouldn't change and
	@# no recompilation should happen).
	@# Note, this only helps on subsequent runs if the kernel sources
	@# haven't actually changed, e.g. if you run 'make run' instead of
	@# 'make rerun'.
	touch $(KERNEL_DOTCONFIG)
	+$(SCHED) $(IONICE) $(KERNEL_MAKE) -j$(PARALLEL) \
	    KBUILD_IMAGE=arch/arm64/boot/Image \
	    all
	@# Image might not change, but we still need to ensure it is
	@# newer than any prerequisites
	touch $(BUILD_STAMP) $(OUT)/linux/arch/arm64/boot/Image

.PHONY: build
build: $(BUILD_STAMP)


DIRS += $(OUT) $(OUT)/dtb $(OUT)/linux $(OUT)/images
$(DIRS):
	mkdir -p $@


$(BOARDDTBS_OUT): $(BUILD_STAMP) | $(OUT)/dtb
	cp --reflink=auto \
	    '$(shell find $(OUT)/linux/arch/arm64/boot/dts/ -name $(notdir $@))' \
	    $(OUT)/dtb

ifneq ($(INTERNAL_PARALLEL_STRIP),)
# find should only run after modules have been installed because otherwise
# the list of files will be incorrect. We re-run make with
# INTERNAL_PARALLEL_STRIP set to ensure that. This way, we can run strip in
# parallel using as many jobs as make deems appropriate.
MODULES_TO_STRIP := $(shell find $(OUT)/initramfs_staging/unstripped/lib/modules -name '*.ko')
STRIPPED_MODULES := $(subst unstripped/,stripped/,$(MODULES_TO_STRIP))

$(STRIPPED_MODULES): $(OUT)/initramfs_staging/stripped/lib/modules/%.ko: $(OUT)/initramfs_staging/unstripped/lib/modules/%.ko
	@$(STRIP) --strip-debug \
	         --remove-section=.comment --remove-section=.note \
		 --preserve-dates -o $@ $<

.PHONY: strip_installed_modules
strip_installed_modules: $(STRIPPED_MODULES)
endif

$(OUT)/.kernel.modules.installed.stamp: $(BUILD_STAMP)
	rm -rf $(OUT)/initramfs_staging
	+$(KERNEL_MAKE) -j$(PARALLEL) \
	    INSTALL_MOD_PATH="../initramfs_staging/unstripped" \
	    modules_install > /dev/null
	mkdir -p $(OUT)/initramfs_staging/stripped/lib/modules
	rsync --archive --hard-links --acls --whole-file --xattrs \
	      --sparse --preallocate --exclude '*.ko' \
	      $(OUT)/initramfs_staging/unstripped/lib/modules/ \
	      $(OUT)/initramfs_staging/stripped/lib/modules
	$(MAKE) --no-print-directory -j$(PARALLEL) \
	    INTERNAL_PARALLEL_STRIP=1 strip_installed_modules
	touch $@

SUPPORTED_COMPRESSIONS := gz xz lz4
gz_cmd = $(KGZIP) --keep --stdout --fast $(1) > $(2)
xz_cmd = xz -T0 --compress --keep --stdout --fast $(1) > $(2)
lz4_cmd = lz4 --compress --keep --stdout --fast $(1) > $(2)

$(OUT)/images/Image.%: $(OUT)/linux/arch/arm64/boot/Image | $(OUT)/images
	$(call $(*)_cmd,$<,$@)

$(OUT)/images/dtb.img: $(EFFECTIVE_BOARDDTB_OUT) $(OUT)/FORCE | $(OUT)/images
	cp --reflink=auto $< $@.tmp
	if ! cmp -s '$@' '$@.tmp' ; then mv '$@.tmp' '$@' ; else rm '$@.tmp' ; fi

$(OUT)/images/vendor-bootconfig.img: | $(OUT)/images
	echo "androidboot.boot_devices=14700000.ufs" > $(OUT)/images/vendor-bootconfig.img

INITRAMFS_CPIO_SOURCES_STAMP := $(OUT)/.initramfs.cpio.source.stamp
$(INITRAMFS_CPIO_SOURCES_STAMP): RELATIVE_OUT_DIR := $(shell realpath --relative-to=prebuilt-busybox-master $(OUT))
$(INITRAMFS_CPIO_SOURCES_STAMP): $(OUT)/FORCE | $(OUT)
	export GIT_INDEX_FILE=$(RELATIVE_OUT_DIR)/$(@F).gitindex \
	&& cd prebuilt-busybox-master \
	&& cp .git/index "$${GIT_INDEX_FILE}" \
	&& git --git-dir=./.git add -A . \
	&& git --git-dir=./.git write-tree > $(RELATIVE_OUT_DIR)/$(@F).tmp \
	&& rm "$${GIT_INDEX_FILE}"
	if ! cmp -s '$@' '$@.tmp' ; then mv '$@.tmp' '$@' ; else rm '$@.tmp' ; fi

PREBUILT_CPIO_BUSYBOX := $(OUT)/images/initramfs.platform.busybox.cpio

$(PREBUILT_CPIO_BUSYBOX): RELATIVE_OUT_DIR = $(shell realpath --relative-to=prebuilt-busybox-master/rootfs $(OUT)/images)
$(PREBUILT_CPIO_BUSYBOX): $(INITRAMFS_CPIO_SOURCES_STAMP) | $(OUT)/images
	cd prebuilt-busybox-master/rootfs \
	&& find . -print0 \
	   | sort -z \
	   | cpio --null -H newc -o \
	   > $(RELATIVE_OUT_DIR)/$(@F)

INITRAMFS_PLATFORM_CPIO_LOCATION_STAMP := $(OUT)/.initramfs.platform.cpio.location.stamp
$(INITRAMFS_PLATFORM_CPIO_LOCATION_STAMP): $(OUT)/FORCE | $(OUT)
	echo $(PREBUILT_CPIO) > '$@.tmp'
	if ! cmp -s '$@' '$@.tmp' ; then mv '$@.tmp' '$@' ; else rm '$@.tmp' ; fi

$(OUT)/images/initramfs.dlkm.cpio: $(OUT)/.kernel.modules.installed.stamp
$(OUT)/images/initramfs.dlkm.cpio: | $(OUT)/images
	$(MKBOOTFS) $(OUT)/initramfs_staging/stripped > '$@.tmp'
	mv '$@.tmp' '$@'

$(OUT)/images/initramfs.dlkm.cpio.%: $(OUT)/images/initramfs.dlkm.cpio | $(OUT)/images
	$(call $(*)_cmd,$<,$@)

POSSIBLE_PLATFORM_CPIOS := $(foreach compr,$(SUPPORTED_COMPRESSIONS),$(OUT)/images/initramfs.platform.cpio.$(compr))
$(POSSIBLE_PLATFORM_CPIOS): $(OUT)/images/initramfs.platform.cpio.%: $(INITRAMFS_PLATFORM_CPIO_LOCATION_STAMP)
$(POSSIBLE_PLATFORM_CPIOS): $(OUT)/images/initramfs.platform.cpio.%: $(PREBUILT_CPIO) | $(OUT)/images
	if ! file --brief --dereference '$<' | grep -q "compressed data" ; then \
	    $(call $(*)_cmd,$<,$@) ;\
	else \
	    cp --reflink=auto '$<' '$@' ;\
	fi

# we also add $(OUT)/FORCE in case it's passed in from the environment
$(OUT)/.kernelcmdline: $(OUT)/FORCE $(THIS_MAKEFILE)
	echo $(KERNELCMDLINE) > $@.tmp
	if ! cmp -s '$@' '$@.tmp' ; then mv '$@.tmp' '$@' ; else rm '$@.tmp' ; fi

FLASHABLE_IMAGES ?= $(OUT)/images/boot.img $(OUT)/images/vendor_boot.img

$(OUT)/images/boot.img: $(OUT)/images/Image.lz4
$(OUT)/images/vendor_boot.img: $(OUT)/.kernelcmdline
$(OUT)/images/vendor_boot.img: $(OUT)/images/dtb.img
$(OUT)/images/vendor_boot.img: $(OUT)/images/initramfs.dlkm.cpio.gz
$(OUT)/images/vendor_boot.img: $(OUT)/images/initramfs.platform.cpio.gz
$(OUT)/images/vendor_boot.img: $(OUT)/images/vendor-bootconfig.img
# &: requires GNU Make 4.3 or newer (Rules with Grouped Targets)
$(OUT)/images/boot.img $(OUT)/images/vendor_boot.img &: | $(OUT)/images
	$(MKBOOTIMG) --header_version 4 \
	    --dtb $(OUT)/images/dtb.img \
	    --kernel $(OUT)/images/Image.lz4 \
	    --vendor_bootconfig $(OUT)/images/vendor-bootconfig.img \
	    --vendor_cmdline "$(KERNELCMDLINE)" \
	    --ramdisk_type platform \
	    --ramdisk_name PLAT \
	    --vendor_ramdisk_fragment $(OUT)/images/initramfs.platform.cpio.gz \
	    --ramdisk_type dlkm \
	    --ramdisk_name DLKM \
	    --vendor_ramdisk_fragment $(OUT)/images/initramfs.dlkm.cpio.gz \
	    \
	    --vendor_boot $(OUT)/images/vendor_boot.img \
	    --output $(OUT)/images/boot.img

# these are the images that can (and need to) be flashed
.PHONY: images
images: $(FLASHABLE_IMAGES)


# actual vendors, keep sorted
DT_BINDING_CHECK_VENDORS := exynos google samsung
# additional bindings used by Pixel, keep sorted
DT_BINDING_CHECK_VENDORS += max77759 syscon
DT_BINDING_CHECK_VENDOR_TARGETS := $(foreach v,$(DT_BINDING_CHECK_VENDORS),dt_binding_check_vendor_$(v))
DTBS_CHECK_ALL_TARGETS := dt_binding_check $(DT_BINDING_CHECK_VENDOR_TARGETS) dt_binding_check_vendors
DTBS_CHECK_ALL_TARGETS += dtbs_check_raviole dtbs_check
DTBS_CHECK_ALL_TARGETS += dtbs_check_exynos32 dtbs_check_exynos64

.PHONY: $(DTBS_CHECK_ALL_TARGETS)
$(DTBS_CHECK_ALL_TARGETS): $(KERNEL_STAMP)

$(OUT)/linux/scripts/dtc/dtc: $(KERNEL_STAMP) $(KERNEL_DOTCONFIG)
	+$(KERNEL_MAKE) -j$(PARALLEL) scripts_dtc

DT_SCHEMA_FILE := $(OUT)/linux/Documentation/devicetree/bindings/processed-schema.json
$(DT_SCHEMA_FILE): $(KERNEL_STAMP) $(KERNEL_DOTCONFIG) $(OUT)/FORCE
	+$(KERNEL_MAKE) -j$(PARALLEL) dt_binding_schemas

DT_SCHEMA_FILE32 := $(OUT)/linux32/Documentation/devicetree/bindings/processed-schema.json
$(DT_SCHEMA_FILE32): $(KERNEL_STAMP) $(KERNEL_DOTCONFIG32) $(OUT)/FORCE
	+$(KERNEL_MAKE) -j$(PARALLEL) ARCH=arm O=$(KERNEL_OUT)/linux32 dt_binding_schemas

refcheckdocs: src/pixelscripts/refcheckdocs.ignore
	@# the script never exits with an error, while grep will exit with
	@# error if no match was found, i.e. the documentation is clean. We
	@# therefore need to invert grep's exit code.
	(cd $(LINUX) \
	 && scripts/documentation-file-ref-check 2>&1) \
	| grep -vFf src/pixelscripts/refcheckdocs.ignore && rc=1 || rc=0 ;\
	exit $$rc

dt_compatible_check dtbs_check: $(OUT)/linux/scripts/dtc/dtc $(DT_SCHEMA_FILE) $(KERNEL_DOTCONFIG)
	+$(KERNEL_MAKE) -j$(PARALLEL) W=1 $@

dtbs_check_exynos32: $(DT_SCHEMA_FILE32) $(KERNEL_DOTCONFIG32)
	+$(KERNEL_MAKE) -j$(PARALLEL) ARCH=arm O=$(KERNEL_OUT)/linux32 \
	    W=1 DT_CHECKER_FLAGS=-m CHECK_DTBS=y \
	    $$(set -x ; find $(LINUX)/arch/arm/boot/dts -path '*/samsung/*.dts' \
	       | sed -e 's|^$(LINUX)/arch/arm[^/]*/boot/dts/||' \
	             -e 's|\.dts$$|.dtb|')

dtbs_check_exynos64: $(OUT)/linux/scripts/dtc/dtc $(DT_SCHEMA_FILE) $(KERNEL_DOTCONFIG)
	+$(KERNEL_MAKE) -j$(PARALLEL) \
	    W=1 DT_CHECKER_FLAGS=-m CHECK_DTBS=y \
	    $$(set -x ; find $(LINUX)/arch/arm64/boot/dts \
	                    \( -name '*exynos*.dts' -o \
	                       -path '*/apple/*.dts' -o \
	                       -path '*/tesla/*.dts' \) \
	       | sed -e 's|^$(LINUX)/arch/arm[^/]*/boot/dts/||' \
	             -e 's|\.dts$$|.dtb|')

dtbs_check_exynos: dtbs_check_exynos32 dtbs_check_exynos64

dtbs_check_raviole: W ?= 1
dtbs_check_raviole: $(OUT)/linux/scripts/dtc/dtc $(DT_SCHEMA_FILE) $(KERNEL_DOTCONFIG)
	@# remove existing DTBs, so kbuild reruns dt-validate
	rm -rf $(OUT)/linux/arch/arm64/boot/dts/exynos/google
	+$(KERNEL_MAKE) -j$(PARALLEL) \
	    W=$(W) DT_CHECKER_FLAGS=-m CHECK_DTBS=y \
	    $$(find $(LINUX)/arch/arm64/boot/dts -name 'gs101-*.dts' \
	       | sed -e 's|^$(LINUX)/arch/arm64/boot/dts/||' \
	             -e 's|\.dts$$|.dtb|')

dt_binding_check: $(OUT)/linux/scripts/dtc/dtc $(DT_SCHEMA_FILE)
	+$(KERNEL_MAKE) -j$(PARALLEL) \
	    W=2 DT_CHECKER_FLAGS=-m $@

$(DT_BINDING_CHECK_VENDOR_TARGETS): $(OUT)/linux/scripts/dtc/dtc $(DT_SCHEMA_FILE)
	+$(KERNEL_MAKE) -j$(PARALLEL) \
	    W=2 DT_CHECKER_FLAGS=-m \
	    DT_SCHEMA_FILES=$(subst dt_binding_check_vendor_,,$@) \
	    dt_binding_check

dt_binding_check_vendors: $(OUT)/linux/scripts/dtc/dtc $(DT_SCHEMA_FILE)
	+$(KERNEL_MAKE) -j$(PARALLEL) \
	    W=2 DT_CHECKER_FLAGS=-m \
	    DT_SCHEMA_FILES=$(shell echo $(DT_BINDING_CHECK_VENDORS) | sed 's| \+|:|g') \
	    dt_binding_check

.PHONY: validate_dt
validate_dt: dt_binding_check_vendors dtbs_check_raviole

.PHONY: validate_dt2
validate_dt2: dt_binding_check dtbs_check



.PHONY: prepare-device
prepare-device:
	fastboot oem disable-verity
	fastboot oem disable-verification
	fastboot oem uart enable
	#fastboot oem uart disable
	fastboot oem uart config 3000000
	fastboot oem ramdump disable
	@# ensure no previous state in case flashing goes wrong in any way
	fastboot erase boot
	fastboot erase dtbo
	fastboot erase vendor_boot

.PHONY: flash flash-no-boot reflash reflash-no-boot
flash-no-boot: $(FLASHABLE_IMAGES)
flash-no-boot reflash-no-boot: prepare-device
flash-no-boot reflash-no-boot:
	fastboot flash vendor_boot $(OUT)/images/vendor_boot.img
	fastboot flash boot $(OUT)/images/boot.img

flash reflash:
	$(MAKE) --no-print-directory $@-no-boot
	fastboot continue

.PHONY: run rerun
run: $(FLASHABLE_IMAGES)
run rerun:
	fastboot stage $(OUT)/images/vendor_boot.img
	fastboot boot $(OUT)/images/boot.img

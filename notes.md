
# Running downstream Pixel drivers with Yocto rootfs

Sometimes it can be useful to run the downstream Pixel drivers with the Yocto
rootfs to have a more enriched development environment with standard development
tools.

Do the usual Yocto rootfs build

```markdown
MACHINE=google-gs DISTRO=poky EXTRA_LAYERS='' . ./setup-environment /yocto-builds/yocto/build-google-gs-poky
umask 0022
bitbake virtual/kernel core-image-full-cmdline
```

Follow the usual instructions for checking out the downstream Pixel drivers
here https://source.android.com/docs/setup/build/building-pixel-kernels

Enable the following additonal kernel options in gki_defconfig

```markdown
diff --git a/arch/arm64/configs/gki_defconfig b/arch/arm64/configs/gki_defconfig
index a13af654f0fa..e597861853c4 100644
--- a/arch/arm64/configs/gki_defconfig
+++ b/arch/arm64/configs/gki_defconfig
@@ -96,6 +96,7 @@ CONFIG_JUMP_LABEL=y
 CONFIG_SHADOW_CALL_STACK=y
 CONFIG_CFI_CLANG=y
 CONFIG_MODULES=y
+CONFIG_MODULE_FORCE_LOAD=y
 CONFIG_MODULE_UNLOAD=y
 CONFIG_MODVERSIONS=y
 CONFIG_MODULE_SCMVERSION=y
@@ -312,6 +313,8 @@ CONFIG_PCIE_KIRIN=y
 CONFIG_PCIE_DW_PLAT_EP=y
 CONFIG_PCIE_QCOM=y
 CONFIG_PCI_ENDPOINT=y
+CONFIG_DEVTMPFS=y
+CONFIG_DEVTMPFS_MOUNT=y
 CONFIG_FW_LOADER_USER_HELPER=y
 # CONFIG_FW_CACHE is not set
 CONFIG_REGMAP_KUNIT=m

```

Build the downstream Pixel drivers, and then manually re-package the boot
images with the Yocto initramfs.

```markdown
tools/bazel run --config=fast --config=stamp //private/google-modules/soc/gs:slider_dist
cd out/slider/dist
../../../tools/mkbootimg/mkbootimg.py --header_version 4 --dtb dtb.img --kernel Image.lz4 --vendor_bootconfig vendor-bootconfig.img --vendor_cmdline "debug rcupdate.rcu_expedited=1 rcu_nocbs=0-7 earlycon=exynos4210,mmio32,0x10A00000 clocksource=arch_sys_counter androidboot.hardware.platform=gs101 androidboot.debug_level=0x4948 firmware_class.path=/vendor/firmware reserve-fimc=0xffffff90f9fe0000 loop.max_part=7 coherent_pool=4M no_console_suspend softlockup_panic=1 cgroup_disable=memory fw_devlink=on deferred_probe_timeout=30 shell-debug init_fatal_sh root=PARTLABEL=userdata copy_modules" --ramdisk_type platform --ramdisk_name PLAT --vendor_ramdisk_fragment /yocto-builds/yocto/build-google-gs-poky/tmp/deploy/images/google-gs/googlegs-initramfs-image-google-gs.cpio.gz --ramdisk_type dlkm --ramdisk_name DLKM --vendor_ramdisk_fragment initramfs.img   --vendor_boot yocto-vendor_boot.img --output yocto-boot.img

```

Flash the artifacts. This initially flashes the Android boot image so that it can
boot into fastbootd to flash vendor_dlkm. We then flash the yocto-boot.img and
yocto-vendor-boot.img

```markdown
fastboot flash boot out/slider/dist/boot.img
fastboot flash vendor_boot out/slider/dist/vendor_boot.img
fastboot flash dtbo out/slider/dist/dtbo.img
fastboot reboot fastboot
fastboot flash vendor_dlkm out/slider/dist/vendor_dlkm.img
fastboot flash boot out/slider/dist/yocto-boot.img
fastboot flash vendor_boot out/slider/dist/yocto-vendor_boot.img

```

For some reason not yet fully debugged, some modules like `samsung_iommu` don't
load which causes a -EPROBE_DEFER timeout 30s after boot which casues MFC driver
to crash and reboot the system. Force load samsung_iommu driver so its depdencies
also probe.

```markdown
modprobe -f samsung_iommu
```

# Upstream Clock Naming

As some of the clock register names are very very long, after some upstream
review on the initial series we came up with a clock name mangling strategy.

The intention was to always still maintain a unique name for every clock in the
system, whilst removing some of the more repetitive sub-strings. Whilst we mangle
the clock name, we keep the original register name unmangled so it is still
searchable in the datasheet.

Andre came up with the following sed regular expressions to help convert datasheet
register names into Linux clock names. They take a text file with a register name
on each line only.

Creating the initial register name text file of register names for a cmu is still
currently a copy/paste excercise from the cmu datasheet pdf.

For example a cmu_hsi2-regs.txt containing following register names

```markdown
CLK_CON_GAT_GOUT_BLK_HSI2_UID_PCIE_GEN4_1_IPCLKPORT_PCIE_003_G4X2_DWC_PCIE_CTL_INST_0_DBI_ACLK_UG
CLK_CON_GAT_GOUT_BLK_HSI2_UID_PCIE_GEN4_1_IPCLKPORT_PCIE_003_G4X2_DWC_PCIE_CTL_INST_0_MSTR_ACLK_UG
CLK_CON_GAT_GOUT_BLK_HSI2_UID_PCIE_GEN4_1_IPCLKPORT_PCIE_003_G4X2_DWC_PCIE_CTL_INST_0_SLV_ACLK_UG
CLK_CON_GAT_GOUT_BLK_HSI2_UID_PCIE_GEN4_1_IPCLKPORT_PCIE_003_PCIE_SUB_CTRL_INST_0_I_DRIVER_APB_CLK
CLK_CON_GAT_GOUT_BLK_HSI2_UID_PCIE_GEN4_1_IPCLKPORT_PCIE_004_G4X1_DWC_PCIE_CTL_INST_0_DBI_ACLK_UG
CLK_CON_GAT_GOUT_BLK_HSI2_UID_PCIE_GEN4_1_IPCLKPORT_PCIE_004_G4X1_DWC_PCIE_CTL_INST_0_MSTR_ACLK_UG
CLK_CON_GAT_GOUT_BLK_HSI2_UID_PCIE_GEN4_1_IPCLKPORT_PCIE_004_G4X1_DWC_PCIE_CTL_INST_0_SLV_ACLK_UG
CLK_CON_GAT_GOUT_BLK_HSI2_UID_PCIE_GEN4_1_IPCLKPORT_PCIE_004_PCIE_SUB_CTRL_INST_0_I_DRIVER_APB_CLK
CLK_CON_GAT_GOUT_BLK_HSI2_UID_PCIE_GEN4_1_IPCLKPORT_PCS_PMA_INST_0_PHY_UDBG_I_APB_PCLK
CLK_CON_GAT_GOUT_BLK_HSI2_UID_PCIE_GEN4_1_IPCLKPORT_PCS_PMA_INST_0_PIPE_PAL_PCIE_INST_0_I_APB_PCLK
CLK_CON_GAT_GOUT_BLK_HSI2_UID_PCIE_GEN4_1_IPCLKPORT_PCS_PMA_INST_0_SF_PCIEPHY210X2_LN05LPE_QCH_TM_WRAPPER_INST_0_I_APB_PCLK
```

You can run the following sed script to convert the clock names

```markdown
sed \
        -e 's|^PLL_LOCKTIME_PLL_\([^_]\+\)|fout_\L\1_pll|' \
        \
        -e 's|^PLL_CON0_MUX_CLKCMU_\([^_]\+\)_\(.*\)|mout_\L\1_\2|' \
        -e 's|^PLL_CON0_PLL_\(.*\)|mout_pll_\L\1|' \
        -e 's|^CLK_CON_MUX_MUX_CLK_\(.*\)|mout_\L\1|' \
        -e '/^PLL_CON[1-4]_[^_]\+_/d' \
        -e '/^[^_]\+_CMU_[^_]\+_CONTROLLER_OPTION/d' \
        -e '/^CLKOUT_CON_BLK_[^_]\+_CMU_[^_]\+_CLKOUT0/d' \
        \
        -e 's|_IPCLKPORT||' \
        -e 's|_RSTNSYNC||' \
        -e 's|_G4X2_DWC_PCIE_CTL||' \
        -e 's|_G4X1_DWC_PCIE_CTL||' \
        -e 's|_PCIE_SUB_CTRL||' \
        -e 's|_INST_0||g' \
        -e 's|_LN05LPE||' \
        -e 's|_TM_WRAPPER||' \
        -e 's|_SF||' \
        \
        -e 's|^CLK_CON_DIV_DIV_CLK_\([^_]\+\)_\(.*\)|dout_\L\1_\2|' \
        \
        -e 's|^CLK_CON_BUF_CLKBUF_\([^_]\+\)_\(.*\)|gout_\L\1_\2|' \
        -e 's|^CLK_CON_GAT_CLK_BLK_\([^_]\+\)_UID_\(.*\)|gout_\L\1_\2|' \
        -e 's|^gout_[^_]\+_[^_]\+_cmu_\([^_]\+\)_pclk$|gout_\1_\1_pclk|' \
        -e 's|^CLK_CON_GAT_GOUT_BLK_\([^_]\+\)_UID_\(.*\)|gout_\L\1_\2|' \
        -e 's|^CLK_CON_GAT_CLK_\([^_]\+\)_\(.*\)|gout_\L\1_clk_\L\1_\2|' \
        \
        -e '/^\(DMYQCH\|PCH\|QCH\|QUEUE\)_/d' cmu-hsi2-regs.txt > cmu-hsi2-names.txt

```

Which gives the following clock names for the example above

```markdown
gout_hsi2_pcie_gen4_1_pcie_003_dbi_aclk_ug
gout_hsi2_pcie_gen4_1_pcie_003_mstr_aclk_ug
gout_hsi2_pcie_gen4_1_pcie_003_slv_aclk_ug
gout_hsi2_pcie_gen4_1_pcie_003_i_driver_apb_clk
gout_hsi2_pcie_gen4_1_pcie_004_dbi_aclk_ug
gout_hsi2_pcie_gen4_1_pcie_004_mstr_aclk_ug
gout_hsi2_pcie_gen4_1_pcie_004_slv_aclk_ug
gout_hsi2_pcie_gen4_1_pcie_004_i_driver_apb_clk
gout_hsi2_pcie_gen4_1_pcs_pma_phy_udbg_i_apb_pclk
gout_hsi2_pcie_gen4_1_pcs_pma_pipe_pal_pcie_i_apb_pclk
gout_hsi2_pcie_gen4_1_pcs_pma_pciephy210x2_qch_i_apb_pclk
```

The second sed script mangles the clock id macros

```markdown
    sed \
        -e 's|^PLL_LOCKTIME_PLL_\([^_]\+\)|CLK_FOUT_\1_PLL|' \
        \
        -e 's|^PLL_CON0_MUX_CLKCMU_\([^_]\+\)_|CLK_MOUT_\1_|' \
        -e 's|^PLL_CON0_PLL_\(.*\)|CLK_MOUT_PLL_\1|' \
        -e 's|^CLK_CON_MUX_MUX_CLK_\(.*\)|CLK_MOUT_\1|' \
        -e '/^PLL_CON[1-4]_[^_]\+_/d' \
        -e '/^[^_]\+_CMU_[^_]\+_CONTROLLER_OPTION/d' \
        -e '/^CLKOUT_CON_BLK_[^_]\+_CMU_[^_]\+_CLKOUT0/d' \
        \
        -e 's|_IPCLKPORT||' \
        -e 's|_RSTNSYNC||' \
        -e 's|_G4X2_DWC_PCIE_CTL||' \
        -e 's|_G4X1_DWC_PCIE_CTL||' \
        -e 's|_PCIE_SUB_CTRL||' \
        -e 's|_INST_0||g' \
        -e 's|_LN05LPE||' \
        -e 's|_TM_WRAPPER||' \
        -e 's|_SF||' \
        \
        -e 's|^CLK_CON_DIV_DIV_CLK_\([^_]\+\)_|CLK_DOUT_\1_|' \
        \
        -e 's|^CLK_CON_BUF_CLKBUF_\([^_]\+\)_|CLK_GOUT_\1_|' \
        -e 's|^CLK_CON_GAT_CLK_BLK_\([^_]\+\)_UID_|CLK_GOUT_\1_|' \
        -e 's|^CLK_GOUT_[^_]\+_[^_]\+_CMU_\([^_]\+\)_PCLK$|CLK_GOUT_\1_PCLK|' \
        -e 's|^CLK_CON_GAT_GOUT_BLK_\([^_]\+\)_UID_|CLK_GOUT_\1_|' \
        -e 's|^CLK_CON_GAT_CLK_\([^_]\+\)_\(.*\)|CLK_GOUT_\1_CLK_\1_\2|' \
        \
        -e '/^\(DMYQCH\|PCH\|QCH\|QUEUE\)_/d' cmu-hsi2-regs.txt > cmu-hsi2-ids.txt
```

From the example input above this gives the following cmu-hsi2-ids.txt output.

```markdown
CLK_GOUT_HSI2_PCIE_GEN4_1_PCIE_003_DBI_ACLK_UG
CLK_GOUT_HSI2_PCIE_GEN4_1_PCIE_003_MSTR_ACLK_UG
CLK_GOUT_HSI2_PCIE_GEN4_1_PCIE_003_SLV_ACLK_UG
CLK_GOUT_HSI2_PCIE_GEN4_1_PCIE_003_I_DRIVER_APB_CLK
CLK_GOUT_HSI2_PCIE_GEN4_1_PCIE_004_DBI_ACLK_UG
CLK_GOUT_HSI2_PCIE_GEN4_1_PCIE_004_MSTR_ACLK_UG
CLK_GOUT_HSI2_PCIE_GEN4_1_PCIE_004_SLV_ACLK_UG
CLK_GOUT_HSI2_PCIE_GEN4_1_PCIE_004_I_DRIVER_APB_CLK
CLK_GOUT_HSI2_PCIE_GEN4_1_PCS_PMA_PHY_UDBG_I_APB_PCLK
CLK_GOUT_HSI2_PCIE_GEN4_1_PCS_PMA_PIPE_PAL_PCIE_I_APB_PCLK
CLK_GOUT_HSI2_PCIE_GEN4_1_PCS_PMA_PCIEPHY210X2_QCH_I_APB_PCLK
```

Two example patches with this name mangling are
https://lore.kernel.org/all/20240423-hsi0-gs101-v1-3-2c3ddb50c720@linaro.org/ and
https://lore.kernel.org/all/20240423-hsi0-gs101-v1-1-2c3ddb50c720@linaro.org/

